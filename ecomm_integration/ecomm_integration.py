from openerp import models, fields


class PartnerInherit(models.Model):
    _inherit = "res.partner"

    ecomm_id = fields.Integer()


class ProductCategoryInherit(models.Model):
    _inherit = "product.category"

    ecomm_id = fields.Integer()


class ProductProductInherit(models.Model):
    _inherit = "product.product"

    ecomm_id = fields.Integer()

class SaleOrderInherit(models.Model):
    _inherit = "sale.order"

    ecomm_id = fields.Integer()